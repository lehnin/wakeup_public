var runs =0;
var results = 0;
var isExam = new Boolean();

 
dwv.gui.getElement = dwv.gui.base.getElement;
dwv.gui.displayProgress = function (percent) {};

var activeLoads = 0;
 
var app = new dwv.App();
var arr;
 
app.init({

    "containerDivId": "dwv",
   "tools" : ["Scroll","WindowLevel","ZoomAndPan"],  
   "isMobile" : true,
    "fitToWindow": true,


});
 
 

 
app.addEventListener("load-end", function (event) {
    document.getElementById("tools").disabled = false;
 
   
    activeLoads -=1;
    if(activeLoads==0){
        allbuttons = document.getElementsByTagName('button');
        for (var i=0; i<allbuttons.length;i++){allbuttons[i].disabled = false;  }
    }

});



    document.addEventListener("DOMContentLoaded", function ( ) {
        domContentLoaded = true;
   
    });
 

 
var patt1=/.*\.dcm+$/i;
var re = new RegExp("/.*\.dcm+$/i");

 
var app2 = new dwv.App();

app2.init({
    "containerDivId": "dwv2",

    "tools" : ["Scroll","WindowLevel","ZoomAndPan"],
    "isMobile" : true,
    "fitToWindow": true,

});

app2.addEventListener("load-end", function (event) {
    document.getElementById("tools2").disabled = false;
 
  
    activeLoads -=1;
    if(activeLoads==0){
        allbuttons = document.getElementsByTagName('button');
        for (var i=0; i<allbuttons.length;i++){allbuttons[i].disabled = false; }
    }
});







function LoadFile(quiz) {
    var oFrame = document.getElementById("txtFile");
    var strRawContents = oFrame.contentWindow.document.body.childNodes[0].innerHTML;

     arr = strRawContents.split("\r\n");
   
   
   
   

   
     
       
     


 
}


var  gset = 1;
var  ca = 1;

function buildQuiz() {
     
    const output = [];
    var qc = 0;





    as[(gset-1)].forEach((currentQuestion, questionNumber) => {
         
        const answers = [];
         

         
         
         
         
        answers.push(
            `<label>
                       DWI lesion: 
             <input type="radio" name="${questionNumber}:DWI" value="yes">
               Yes
               </label>
               <label>
              <input type="radio" name="${questionNumber}:DWI" value="no">
              No
           </label> 
               <input type="checkbox" name="${questionNumber}:DWIL" value="left">
              Left
           </label> <input type="checkbox" name="${questionNumber}:DWIR" value="right">
              Right
           </label>
           <br>  
           DWI details:
           <label>
          
                 
                     Extended infarct
             <label>
              <input type="radio" name="${questionNumber}:Extended" value="yes">
             Yes
           </label> 
           <label>
              <input type="radio" name="${questionNumber}:Extended" value="no">
             No
           </label> 
         
           
            <label> <input type="checkbox" name="${questionNumber}:hemorrhagic" value="hemorrhagic">
              hemorrhagic transformation
           </label>
           
           <br>  <br>
                   
          
           
             <label>
                        Flair lesion:
             <input type="radio" name="${questionNumber}:FLAIR" value="yes">
             Yes - FLAIR positive
               </label>
             <label>
              <input type="radio" name="${questionNumber}:FLAIR" value="no">
              No - FLAIR negative
              </label>
              <br>
                    Rating impossible due to:
              <label>
              <input type="radio" name="${questionNumber}:FLAIR" value="artifacts">
              Artifacts
              </label>
               <label>
              <input type="radio" name="${questionNumber}:FLAIR" value="leukoaraiosis">
                            overlapping leukoaraiosis

              </label> 
              <label>
              <input type="radio" name="${questionNumber}:FLAIR" value="noDWI">
               no DWI lesion
              </label>
              
           </label>
           <br>  <br>
           <label>
                        Final decision:
             <input type="radio" name="${questionNumber}:Final" value="yes">
               Treat 
               </label><label>
              <input type="radio" name="${questionNumber}:Final" value="no">
              Do not treat
           </label><br>`
        );
         
         
        var q =questionNumber+1;
         

        

        if (isExam === true){
            output.push(
                `<div class="slide">
           <div id="exammode"> Exam mode</div>
           <br>
           <div class="answers"> ${answers.join("")} </div>
         </div>`
            );
        }
        else {
            output.push(
                `<div class="slide">
           <div class="question">Set ${gset}  Case ${q} </div>
           <br>
           <div class="answers"> ${answers.join("")} </div>
         </div>`
            );
        }
    });

     
    quizContainer.innerHTML = output.join("");
}

function showResults() {
     
 

     
    var numCorrect = 0;

    numCorrect= checkResults();
    results = numCorrect + results;


    if (isExam===true ){
        resultsContainer.innerHTML = `${numCorrect} out of ${as[(gset - 1)].length}`;
        if ( runs ===3) {
            resultsContainer.innerHTML = `${numCorrect} out of ${as[(gset - 1)].length} <br> Final result ${results} out of 36 `;

        }
    }
    else {

        if(numCorrect===13){
            resultsContainer.innerHTML = `All of your answers are correct! `;

        }
      else {
            resultsContainer.innerHTML = `Some of your answers are wrong! `;

        }
    }
}


function showSlide(n) {
    slides[currentSlide].classList.remove("active-slide");
    slides[n].classList.add("active-slide");
    currentSlide = n;



    pick(gset ,currentSlide+1);
    if (currentSlide === 0) {
        previousButton.style.display = "none";
    } else {
        previousButton.style.display = "inline-block";
    }

    if (isExam=== true){
        ExamButton.style.display = "none";
        submitButton.style.display = "none";

        trainingButton.style.display = "inline-block";



    }

    else{
        trainingButton.style.display = "none";

        ExamButton.style.display = "inline-block";
    }

    if (currentSlide === slides.length - 1) {
        nextButton.style.display = "none";
         submitButton.style.display = "inline-block";
       
        nextSetButton.style.display = "inline-block";
    } else {
        nextButton.style.display = "inline-block";
 
         
         
    }



}

function showNextSlide() {
    showSlide(currentSlide + 1);

     
}

function showPreviousSlide() {

    showSlide(currentSlide - 1);
     

}



const quizContainer = document.getElementById("quiz");
const awnsersContainer = document.getElementById("awnsers");
const resultsContainer = document.getElementById("results");
const submitButton = document.getElementById("submit");
const trainingButton = document.getElementById("Training");

const questions = document.getElementById("questions");

const nextSetButton = document.getElementById("nextSet");
const ExamButton = document.getElementById("Exam");
const ExamMode = document.getElementById("exammode");

 
buildQuiz();

const previousButton = document.getElementById("previous");
const nextButton = document.getElementById("next");
 slides = document.querySelectorAll(".slide");
let currentSlide = 0;

showSlide(0);

 
submitButton.addEventListener("click", showResults);
previousButton.addEventListener("click", showPreviousSlide);
nextButton.addEventListener("click", showNextSlide);


trainingButton.addEventListener("click", backToTest);
nextSetButton.addEventListener("click", showNextSet);
ExamButton.addEventListener("click", exam);



function checkResults(results) {
     
    const answerContainers2 = quizContainer.querySelectorAll(".answers");
    var numCorrects= 0;
     
    let numSub = 0;
    console.log(as[(gset-1)]);
    for (var index = 0; index < answerContainers2.length; index++) {
        console.log(answerContainers2[index]);
    }
     
    as[(gset-1)].forEach((currentQuestion, questionNumber) => {

        let numSub = 0;

        console.log("curr"+currentQuestion);
        console.log("quest"+questionNumber);
        console.log("answerContainers2"+answerContainers2[0].toString());

         
        let answerContainer = answerContainers2[questionNumber];
           
          

        let selector = "input[name*=DWI]:checked";
        let userAnswer = (answerContainer.querySelector(selector) || {}).value;
        console.log("userAnswer"+userAnswer);
         



         

        if (userAnswer === currentQuestion[0]) {
             
            numSub++;



           answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
                  answerContainers2[questionNumber ].style.color = "red";

        }


         
         
         
         
        selector = "input[name*=DWIL]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
        if (typeof userAnswer === "undefined"){
            userAnswer = null;
        }
        console.log("4"+userAnswer);

         

        if (userAnswer == currentQuestion[4]) {
             

                        numSub++;

           answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
                        answerContainers2[questionNumber].style.color = "red";


        }


        selector = "input[name*=DWIR]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
        if (typeof userAnswer === "undefined"){
            userAnswer = null;
        }
        console.log("5user"+userAnswer);
        console.log("5ref"+currentQuestion[5]);


         

        if (userAnswer === currentQuestion[5] ) {
             
            numSub++;


           answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
             answerContainers2[questionNumber].style.color = "red";


        }
        selector = "input[name*=hemorrhagic]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
         
        if (typeof userAnswer === "undefined"){
            userAnswer = null;
        }

         console.log("hemo"+userAnswer);

         

        if (userAnswer === currentQuestion[6]) {
             
            numSub++;


           answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
         answerContainers2[questionNumber].style.color = "red";

        }

        selector = "input[name*=Extended]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
        console.log("7"+userAnswer);

        if (typeof userAnswer === "undefined"){
            userAnswer = null;
        }

        if (userAnswer === currentQuestion [7]) {
             
            numSub++;


           answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
            answerContainers2[questionNumber].style.color = "red";


        }


         
         
         
         
        selector = "input[name*=FLAIR]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
        console.log("1"+userAnswer);
        if (typeof userAnswer === "undefined"){
            userAnswer = null;
        }
         

        if (userAnswer === currentQuestion[1]) {
             
            numSub++;


           answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
             answerContainers2[questionNumber].style.color = "red";

        }

         
         
         
         
        selector = "input[name*=Final]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
        console.log("2"+userAnswer);

        if (typeof userAnswer === "undefined"){
            userAnswer = null;
        }

        if (userAnswer === currentQuestion[2]) {
             
            numSub++;


           answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
            
            answerContainers2[questionNumber].style.color = "red";

        }
        console.log("numSub"+numSub);

        if (numSub === 7){

   
   numCorrects++;     

            console.log("sub 9 = true");
            console.log( answerContainers2[questionNumber]);
            console.log( "quest"+ questionNumber);

            console.log("numCorrects"+ numCorrects);
            answerContainers2[questionNumber].style.color = "lightgreen";


        }
else          {  answerContainers2[questionNumber].style.color = "red"; }




    });

    console.log("numSub"+numSub);
    console.log("numCorrects"+numCorrects);


    console.log(as.toString());

    resultsContainer.style.display = "INHERIT";

    return numCorrects;
     
    


}



function changeResults() {
     
    const answerContainers2 = quizContainer.querySelectorAll(".answers");

     
    let numChanged = 0;
    console.log(as[(gset-1)]);
    console.log(answerContainers2);

     
    as[(gset-1)].forEach((currentQuestion, questionNumber) => {
        console.log("curr"+currentQuestion);
        console.log("quest"+questionNumber);

         
        let answerContainer = answerContainers2[questionNumber];
         
         
        let selector = "input[name*=DWI]:checked";
        let userAnswer = (answerContainer.querySelector(selector) || {}).value;
        console.log("userAnswer"+userAnswer);
        console.log("currentQuestion"+currentQuestion);

         

        if (userAnswer === currentQuestion[0]) {
             


           answerContainers2[questionNumber].style.color = "lightgreen";
            answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
            numChanged++;
            answerContainers2[questionNumber].style.color = "red";
 
        }


         
         
         
         
        selector = "input[name*=DWIL]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
        console.log("4"+userAnswer)

         

        if (userAnswer === currentQuestion[4]) {
             


           answerContainers2[questionNumber].style.color = "lightgreen";
            answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
            numChanged++;
            answerContainers2[questionNumber].style.color = "red";
            as[(gset - 1)][questionNumber][4] = userAnswer;

        }


        selector = "input[name*=DWIR]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
        console.log("5"+userAnswer);

         

        if (userAnswer === currentQuestion[5]) {
             


           answerContainers2[questionNumber].style.color = "lightgreen";
            answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
            numChanged++;
            answerContainers2[questionNumber].style.color = "red";
            as[(gset - 1)][questionNumber][5] = userAnswer;

        }
        selector = "input[name*=hemorrhagic]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
         
         

         

        if (userAnswer === currentQuestion[6]) {
             


           answerContainers2[questionNumber].style.color = "lightgreen";
            answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
            numChanged++;
            answerContainers2[questionNumber].style.color = "red";
            as[(gset - 1)][questionNumber][6] = userAnswer;

        }

        selector = "input[name*=Extended]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
         

         

        if (userAnswer === currentQuestion[7]) {
             


           answerContainers2[questionNumber].style.color = "lightgreen";
            answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
            numChanged++;
            answerContainers2[questionNumber].style.color = "red";
            as[(gset - 1)][questionNumber][7] = userAnswer;

        }


         
         
         
         
        selector = "input[name*=FLAIR]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
         

         

        if (userAnswer === currentQuestion[1]) {
             


           answerContainers2[questionNumber].style.color = "lightgreen";
            answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
            numChanged++;
            answerContainers2[questionNumber].style.color = "red";
            as[(gset - 1)][questionNumber][1] = userAnswer;

        }

         
         
         
         
        selector = "input[name*=Final]:checked";
        userAnswer = (answerContainer.querySelector(selector) || {}).value;
         

         

        if (userAnswer === currentQuestion[2]) {
             


           answerContainers2[questionNumber].style.color = "lightgreen";
            answerContainers2[questionNumber].style.color = "lightgreen";
        } else {
             
             
            numChanged++;
            answerContainers2[questionNumber].style.color = "red";
            as[(gset - 1)][questionNumber][2] = userAnswer;

        }
        console.log(as.toString());
    });

     
     


}
function showNextSet() {


    if (isExam === true ) {
        if ( runs < 3) {
            gset = pickrandom();
            
             

            runs++;

        }
        if (runs === 3){
            nextSetButton.style.display = "none";

        }
    }
    else {
        if (gset !== 5) {
            gset++;
        } else {
            gset = 1;
        }
    }





        buildQuiz();

    slides = document.querySelectorAll(".slide");
      showSlide(ca-1);
    resultsContainer.style.display = "none";
     
}


function run () {
}




(function() {
    run();
})();

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function pick(s,c){






    console.log (s);
    console.log (c);

   
    var replace = "regex";
    var re2 = new RegExp("Set_"+ s  + "/Case_0\?"  + (c) + "/DWI/");
    var re1 = new RegExp("Set_"+ s  + "/Case_0\?"  + (c) + "/FLAIR/");
    var match1 = largeJson.filter(value => re1.test(value));
    var match2 = largeJson.filter(value => re2.test(value));


    app.reset();
    allbuttons = document.getElementsByTagName('button');
    for (var i=0; i<allbuttons.length;i++){allbuttons[i].disabled = true;  }

     
    activeLoads += 1;
    app.loadURLs(match1);


     

     

    app2.reset();

     
    activeLoads +=1;
    app2.loadURLs(match2);
}




function download() {

    let csvContent = "data:text/plain;charset=utf-8," + encodeURIComponent( JSON.stringify(as));
   
    var link = document.createElement("a");
    
   
  


    link.setAttribute("href", csvContent);
    link.setAttribute("download", "awnsers.txt");
    document.body.appendChild(link);  

    link.click();  


}


function exam() {
    isExam = true;
    if (runs <= 3) {
        gset = pickrandom();
        
         
        buildQuiz();
        runs++;
        slides = document.querySelectorAll(".slide");
        showSlide(ca-1);
    }
    else{
        runs= 0;
    }
}
var setarr= [1,2,3,4,5];

function pickrandom(){
        if (setarr.length=== 0 ){setarr= [1,2,3,4,5] ;}
      return   parseInt(setarr.splice(Math.floor(Math.random() * setarr.length),1 ).toString());

}
function backToTest(){
    location.reload();

}